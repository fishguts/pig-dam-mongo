/**
 * User: curtis
 * Date: 12/16/17
 * Time: 2:28 PM
 * Copyright @2017 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("assert");
const fs=require("fs-extra");
const file=require("pig-core").file;


/**
 * Abstracts different means of input
 */
class Input {
	/**
	 * @param {Object} options
	 * @param {String} options.filePath - read from file path
	 * @param {Object} options.object - return this morsel as if input
	 * @param {ReadStream} options.stream - read from stream
	 * @param {Boolean} options.json - treat as json
	 */
	constructor(options) {
		this.options=options;
	}

	/**
	 * Reads data from input as per options configuration
	 * @param {Function} callback
	 * @returns {*}
	 */
	read(callback=undefined) {
		if(this.options.filePath) {
			if(this.options.json) {
				if(callback) {
					file.readToJSON(this.options.filePath, {}, callback);
				} else {
					return file.readToJSONSync(this.options.filePath);
				}
			} else {
				if(callback) {
					fs.readFile(this.options.filePath, callback);
				} else {
					return fs.readFileSync(this.options.filePath);
				}
			}
		} else if(this.options.object) {
			if(callback) {
				process.nextTick(callback, null, this.options.object);
			} else {
				return this.options.object;
			}
		} else if(this.options.stream) {
			Input._readStream(this.options.stream, (error, result)=>{
				if(error) {
					callback(error);
				} else if(this.options.json) {
					try {
						callback(null, JSON.parse(result));
					} catch(error) {
						callback(null, result);
					}
				} else {
					callback(null, result);
				}
			});
		} else {
			throw new Error("type?");
		}
	}

	/********************* Private Interface *********************/
	/**
	 * Reads from specified stream
	 * @param {ReadStream} stream
	 * @param {Function} callback
	 * @private
	 */
	static _readStream(stream, callback) {
		let buffer="";
		assert.ok(callback);
		callback=_.once(callback);
		stream.setEncoding("utf8");
		stream.on("error", callback);
		stream.on("end", ()=>{
			callback(null, buffer);
		});
		stream.on("readable", ()=>{
			const chunk=stream.read();
			if(chunk!==null) {
				buffer+=chunk;
			}
		});
	}
}


/**
 * Abstracts different means of output
 */
class Output {
	/**
	 * @param {Object} options
	 * @param {String} options.filePath - write to file
	 * @param {Boolean} options.devnull - write to space
	 * @param {WriteStream} options.stream - write to stream
	 * @param {Boolean} options.json - treat output as json
	 * @param {Boolean} options.pretty - if json then write with spacing
	 */
	constructor(options) {
		this.options=options;
	}

	/**
	 * Writes data to output as per options configuration
	 * @param {Object|Buffer} data
	 * @param {Function} callback
	 */
	write(data, callback=undefined) {
		// format data if need be
		if(!this.options.devnull) {
			if(this.options.json) {
				if(this.options.pretty) {
					data=JSON.stringify(data, null, "\t");
				} else {
					data=JSON.stringify(data);
				}
			}
			// write data
			if(this.options.filePath) {
				if(callback) {
					fs.writeFile(this.options.filePath, data, callback);
				} else {
					fs.writeFileSync(this.options.filePath, data);
				}
			} else if(this.options.stream) {
				this.options.stream.write(data, callback);
			} else {
				throw new Error("type?");
			}
		}
	}
}


exports.Input=Input;
exports.Output=Output;
