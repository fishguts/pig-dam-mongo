/**
 * User: curt
 * Date: 11/18/2017
 * Time: 4:30 PM
 */

const constant=require("pig-core").constant;

/**
 * @type {@module:pig-core/constant}
 */
module.exports=Object.assign(module.exports, constant);


/**
 * Commit types
 */
exports.commit={
	policy: {
		AUTO: "auto",
		MANUAL: "manual"
	}
};
exports.isValidCommitPolicy=constant.isValidValue.bind(null, exports.commit.policy);

/**
 * Module names in our environment
 */
exports.module={
	DAM: "server",
	FACTORY: "factory",
	MONGO: "mongo",
	SEARCH: "search",
	SETTINGS: "settings"
};
exports.isValidModule=constant.isValidValue.bind(null, exports.module);

/**
 * Known for execution and testing
 */
exports.nodenv={
	TEST: "test",
	LOCAL: "local",
	DEVELOPMENT: "development",
	STAGING: "staging",
	PRODUCTION: "production"
};
exports.isValidNodenv=constant.isValidValue.bind(null, exports.nodenv);

/**
 * URN prefix and friends
 */
exports.urn={
	type: {
		PROJECT: "prj",
		QUEUE: "que",
		CONTENT: "cnt",
		ROUTE: "rte",
		database: {
			DOCUMENT: "db:doc",
			KEYVALUE: "db:key"
		}
	}
};
