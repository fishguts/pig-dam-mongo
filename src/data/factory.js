/**
 * User: curtis
 * Date: 1/3/18
 * Time: 9:06 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const fs=require("fs-extra");
const db_model=require("./model");
const constant=require("../common/constant");
const notification=require("pig-core").notification;


const storage={
	/**
	 * Manages our instances per application: application.local, application.dev, etc.
	 */
	application: {},
	nodenv: (process.env.NODE_ENV || constant.nodenv.LOCAL)
};


/**
 * Gets/Sets application singleton
 */
exports.application={
	/**
	 * Gets a configuration for the specified application
	 * @param {Object} overrides
	 *  - nodenv {String}
	 *  - mode {String}
	 * @returns {Application}
	 */
	get: function(overrides=undefined) {
		let nodenv=storage.nodenv;
		if(_.has(overrides, "nodenv")) {
			nodenv=overrides.nodenv;
			overrides=_.omit(overrides, "nodenv");
		}
		if(storage.application[nodenv]) {
			// the application has already been configured nonetheless we will take overrides
			if(overrides) {
				_.merge(storage.application[nodenv].raw.application, overrides);
			}
		} else {
			exports.application.set(fs.readJSONSync(`./res/configuration/${nodenv}/settings.json`), overrides);
		}
		return storage.application[nodenv];
	},

	/**
	 * Sets a configuration for the specified application. The only time this should need to be called outside of the factory
	 * is when we are retrieving settings from our settings server
	 * @param {Object} settings
	 * @param {Object} overrides
	 * @returns {Application}
	 */
	set: function(settings, overrides=undefined) {
		const nodenv=settings.env.name;
		settings=_.cloneDeep(settings);
		// help keep the differences between apps easier to manage by setting up an "application" domain.
		// Once this is set then the differences in Application per module will be minimized
		settings.application=settings.module.mongo;
		_.merge(settings.application, overrides);
		storage.application[nodenv]=new db_model.Application(settings);
		this._applicationToEnvironment(storage.application[nodenv]);
		if(nodenv===storage.nodenv) {
			notification.emit(constant.event.SETTINGS_LOADED);
		}
		return storage.application[nodenv];
	},

	/**
	 * Aligns the environment to our application
	 * @param {Application} application
	 * @private
	 */
	_applicationToEnvironment: function(application) {
		process.env.DEBUG=application.debug.enabled;
		process.env.VERBOSE=application.debug.verbose;
	}
};
