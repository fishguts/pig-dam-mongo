/**
 * User: curtis
 * Date: 12/8/17
 * Time: 8:05 PM
 * Copyright @2017 by Xraymen Inc.
 */

const assert=require("assert");
const constant=require("../../../src/common/constant");

describe("common.constant", function() {
	it("should validate module properly", function() {
		assert.equal(constant.isValidModule(constant.module.DAM), true);
		assert.equal(constant.isValidModule("false"), false);
	});

	it("should validate nodenv properly", function() {
		assert.equal(constant.isValidNodenv(constant.nodenv.DEVELOPMENT), true);
		assert.equal(constant.isValidNodenv("false"), false);
	});

	it("should validate commit policy properly", function() {
		assert.equal(constant.isValidCommitPolicy(constant.commit.policy.AUTO), true);
		assert.equal(constant.isValidCommitPolicy("false"), false);
	});
});
